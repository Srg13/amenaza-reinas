all: PR
.PHONY: test
pruebas = pruebas
caso1 = A1
caso2 = A8
caso3 = C4
caso4 = D4
caso5 = A6
caso6 = C8
caso7 = F8
caso8 = G2
caso9 = H1
caso10 = H8
ext = _aux.txt

PR: PR.o
	gcc -o PR PR.o
PR.o: PR.c
	gcc -c PR.c amenazasReinas.h
amenazasReinas: amenazasReinas.o
	gcc -o amenazasReinas amenazasReinas.o
amenazasReinas.o: amenazasReinas.c amenazasReinas.o
	gcc -c amenazasReinas.c

clean:
	rm *.o PR $(pruebas)/*_aux.txt
test:
	@echo "Comprobando casos"
	@echo
	@echo "Caso 1"
	@./PR $(caso1) > $(pruebas)/$(caso1)$(ext)
	@diff -c $(pruebas)/$(caso1)$(ext) $(pruebas)/$(caso1).txt || exit 0
	@echo "Test realizado correctamente"
	@echo
	@echo "Caso 2"
	@./PR $(caso2) > $(pruebas)/$(caso2)$(ext)
	@diff -c $(pruebas)/$(caso2)$(ext) $(pruebas)/$(caso2).txt || exit 0
	@echo "Test realizado correctamente"
	@echo
	@echo "Caso 3"
	@./PR $(caso3) > $(pruebas)/$(caso3)$(ext)
	@diff -c $(pruebas)/$(caso3)$(ext) $(pruebas)/$(caso3).txt || exit 0
	@echo "Test realizado correctamente"
	@echo
	@echo "Caso 4"
	@./PR $(caso4) > $(pruebas)/$(caso4)$(ext)
	@diff -c $(pruebas)/$(caso4)$(ext) $(pruebas)/$(caso4).txt || exit 0
	@echo "Test realizado correctamente"
	@echo
	@echo "Caso 5"
	@./PR $(caso5) > $(pruebas)/$(caso5)$(ext)
	@diff -c $(pruebas)/$(caso5)$(ext) $(pruebas)/error$(caso5).txt || exit 0
	@echo "Error"
	@echo
	@echo "Caso 6"
	@./PR $(caso6) > $(pruebas)/$(caso6)$(ext)
	@diff -c $(pruebas)/$(caso6)$(ext) $(pruebas)/error$(caso6).txt || exit 0
	@echo "Error"
	@echo
	@echo "Caso 7"
	@./PR $(caso7) > $(pruebas)/$(caso7)$(ext)
	@diff -c $(pruebas)/$(caso7)$(ext) $(pruebas)/$(caso7).txt || exit 0
	@echo "Test realizado correctamente"
	@echo
	@echo "Caso 8"
	@./PR $(caso8) > $(pruebas)/$(caso8)$(ext)
	@diff -c $(pruebas)/$(caso8)$(ext) $(pruebas)/$(caso8).txt || exit 0
	@echo "Test realizado correctamente"
	@echo
	@echo "Caso 9"
	@./PR $(caso9) > $(pruebas)/$(caso9)$(ext)
	@diff -c $(pruebas)/$(caso9)$(ext) $(pruebas)/$(caso9).txt || exit 0
	@echo "Test realizado correctamente"
	@echo
	@echo "Caso 10"
	@./PR $(caso10) > $(pruebas)/$(caso10)$(ext)
	@diff -c $(pruebas)/$(caso10)$(ext) $(pruebas)/$(caso10).txt || exit 0
	@echo "Test realizado correctamente"
	@echo
