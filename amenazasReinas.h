#include <stdio.h>
#include <stdlib.h>

#define SIZE 8

void iniMatrix(char matrix[SIZE][SIZE]);
void printMatrix(char matrix[SIZE][SIZE]);
int getIndexColumn(char column);
void insertReinaOnMatrix(char matrix[SIZE][SIZE],int file, int column, char c);
void numberOnFile(char matrix[SIZE][SIZE],int file, int column);
void numberOnColumn(char matrix[SIZE][SIZE],int file, int column);
void numberOnAscendingDiagonal(char matrix[SIZE][SIZE], int file, int column);
void numberOnDescendingDiagonal(char matrix[SIZE][SIZE], int file, int column);
