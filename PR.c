//Samantha Roldán García
//27/03/2020

#include "amenazasReinas.h"
#include "amenazasReinas.c"

int main(int argc, char *argv[])
{
    char matrix[SIZE][SIZE];
    int column = getIndexColumn(argv[1][0]);
    int file = argv[1][1] - '0';

    if(column <=SIZE && file<=SIZE)
    {
      iniMatrix(matrix);
      insertReinaOnMatrix(matrix,file,column,'#');
      numberOnFile(matrix,file,column);
      numberOnColumn(matrix,file,column);
      numberOnAscendingDiagonal(matrix,file,column);
      numberOnDescendingDiagonal(matrix,file,column);
      printMatrix(matrix);
    }
    else
    {
      printf("Error.");
    }
}
