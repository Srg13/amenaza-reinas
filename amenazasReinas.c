#include <stdio.h>
#include <stdlib.h>
#include "amenazasReinas.h"

#define SIZE 8

void iniMatrix(char matrix[SIZE][SIZE])
{
  for(int i=0; i<SIZE; i++)
  {
      for(int j=0; j<SIZE; j++)
      {
          matrix[i][j]= '.';
      }
  }
}
void printMatrix(char matrix[SIZE][SIZE])
{
    printf("  ------------------\n");
    for(int i=0; i<=SIZE-1; i++)
    {
      for(int j=0; j<=SIZE-1; j++)
      {
          if(j == 0)
          {
              printf("%d |%c",(SIZE+1)-(i+1), matrix[i][j]);
          }
          else
          {
            printf(" %c", matrix[i][j]);
          }
          if(j == SIZE-1)
          {
              printf(" |");
          }
      }
    printf("\n");
    }
    printf("  ------------------\n");
    printf("   A B C D E F G H \n");
}
int getIndexColumn(char column)
{
  switch (column)
  {
    case 'A':
      return 0;

    case 'B':
      return 1;

    case 'C':
      return 2;

    case 'D':
      return 3;

    case 'E':
      return 4;

    case 'F':
      return 5;

    case 'G':
      return 6;

    case 'H':
      return 7;

    default:
      return 8;
  }
}
void insertReinaOnMatrix(char matrix[SIZE][SIZE],int file, int column, char c)
{
    matrix[SIZE-file][column]=c;
}
void numberOnFile(char matrix[SIZE][SIZE],int file, int column)
{
    for(int j=0; j<SIZE; j++)
    {
        if(j == column)
        {

        }
        else
        {
            matrix[SIZE-file][j]= '1';
        }
    }
}
void numberOnColumn(char matrix[SIZE][SIZE],int file, int column)
{
    for(int i=0; i<SIZE; i++)
    {
        if(SIZE-i == file)
        {

        }
        else
        {
            matrix[i][column]= '1';
        }
    }
}
void numberOnAscendingDiagonal(char matrix[SIZE][SIZE], int file, int column)
{
    int i = SIZE-file;
    int j = column;
    while(i>0 && j<SIZE-1){
      i--;
      j++;
      matrix[i][j]='1';
    }
    i = SIZE-file;
    j = column;
    while(i>0 && j>0){
      i--;
      j--;
      matrix[i][j]='1';
    }
}
void numberOnDescendingDiagonal(char matrix[SIZE][SIZE], int file, int column)
{
    int i = SIZE-file;
    int j = column;
    while(i<SIZE && j<SIZE-1){
      i++;
      j++;
      matrix[i][j]='1';
    }
    i = SIZE-file;
    j = column;
    while(i<SIZE-1 && j>0){
      i++;
      j--;
      matrix[i][j]='1';
    }
}
